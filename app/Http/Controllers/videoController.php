<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\News;
use App\video;
use App\ModNews;
use App\ListNew;
use Session;

class videoController extends Controller
{
    //
  
    public function Listvideos(){
        $lang = Session::get('idlocale');
        $listnews = video::all();
 
        return view('admin.video.news',['lnews'=>$listnews]);
    }
    public function Addvideos(Request $request){
        session(['actionuser' => 'add']);    
  
        $this->validate($request,[
                'newsname' => 'required|unique:news',
                // 'nntagnew' => 'required',
                // 'nntomtatnew' => 'required',
               
                'nnavatarfile' => 'image|max:500000',                
            ],[
                'newsname.required'=> 'Tên bài viết ko được để trống (#)',
                'newsname.unique'=> 'Tên bài viết phải là duy nhất',
                 'video.required'=> 'Video ko được để trống (#)',
                'nntagnew.required'=> 'Tag viết ko được để trống (#)',
                'nnavatarfile.image' => 'Avatar phải là hình ảnh',
                'nnavatarfile.max' => 'Avatar dung lượng quá lớn',

            ]);
        $new = new video;
        $new->newname = $request->newsname;
        $new->newalt = changeTitle(trim($request->newsname));
        $new->slug = changeTitle(trim($request->newsname));
       
        $new->link = $request->link;
        $new->status = $request->nnhide;

          if($request->hasFile('nnavatarfile')){
                $file = $request->file('nnavatarfile');
                $new->newimg = upload_image_post($file,'news');
            }else{
                $new->newimg = "no-img.png";
            }
        //upload video
        if($request->hasFile('video')){
            $video_tmp = Input::file('video'); 
            $video_name = $video_tmp -> getClientOriginalName();
            $video_path = 'public/videos/';
            $video_tmp->move($video_path,$video_name);
            $new -> video = $video_name;
            }
       
        $new->save();
        return back()->with('thongbao','thêm thành công');
    }
    public function Editvideos(Request $request){
        session(['actionuser' => 'edit','editid'=>$request->ennidnews]);   
        $this->validate($request,[

              'newsname' => 'required|unique:news',
                // 'nntagnew' => 'required',
                // 'nntomtatnew' => 'required',
                'video' => 'required',
                'nnavatarfile' => 'image|max:500000',                
            ],[
                'newsname.required'=> 'Tên bài viết ko được để trống (#)',
                'newsname.unique'=> 'Tên bài viết phải là duy nhất',
                 'video.required'=> 'Video ko được để trống (#)',
                'nntagnew.required'=> 'Tag viết ko được để trống (#)',
                'nnavatarfile.image' => 'Avatar phải là hình ảnh',
                'nnavatarfile.max' => 'Avatar dung lượng quá lớn',
            ]);
        $new = video::find($request->ennidnews);
         $new->newname = $request->newsname;
        $new->newalt = changeTitle(trim($request->newsname));
        $new->slug = changeTitle(trim($request->newsname));
       
     
        $new->status = $request->nnhide;

        // dd($new);
        if($request->hasFile('ennavatarfile')){
            $file = $request->file('ennavatarfile');
            // removefile
            $imgold = $request->ennimguserold;
            if($imgold !="no-img.png"){
                while(file_exists("public/img/news/100x100/".$imgold))
                {
                    unlink("public/img/news/100x100/".$imgold);
                    unlink("public/img/news/300x300/".$imgold);
                    unlink("public/img/news/800x800/".$imgold);
                }
            }            
            $new->newimg = upload_image_post($file,'news');
        }
        //upload video
        if($request->hasFile('video')){
            $video_tmp = Input::file('video');
            $video_name = $video_tmp -> getClientOriginalName();
            $video_path = 'public/videos/';
            $video_tmp->move($video_path,$video_name);
            $new -> video = $video_name;
            }
       
        $new->save();
        return back()->with('thongbao','sửa thành công');
    }
    public function Deletevideos(Request $request){
        $new= video::find($request->dennidnew);
        $new->delete();
        $imgold = $request->dennimgnew;
            if($imgold !="no-img.png"){
                while(file_exists("public/img/news/100x100/".$imgold))
                {
                    unlink("public/img/news/100x100/".$imgold);
                    unlink("public/img/news/300x300/".$imgold);
                    unlink("public/img/news/800x800/".$imgold);
                }
            }
        return back()->with('thongbao','Xóa thành công');

    }
}
