@extends('home.master')
@section('title', (!empty($contact)?$contact->seo_title:""))
@section('seo_keyword', (!empty($contact)?$contact->seo_keyword:""))
@section('seo_description', (!empty($contact)?$contact->seo_description:""))
@section('seo_image', (!empty($contact)?asset($contact->seo_image):""))
@section('seo_url', url()->current())
@section('content')



<div class="container">
    <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tìm kiếm</li>
  </ol>
</nav>
</div>

     <div class="main-content-wrapper section-padding-100">
        <div class="container">
            <div class="row justify-content-center">
                <!-- ============= Post Content Area Start ============= -->
                <div class="col-12 col-lg-8">
                    <div class="post-content-area mb-100">
                        <!-- Catagory Area -->
                        <div class="world-catagory-area">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                            
                                <li class="title">
                                	@if(isset($key))
										<h2><i class="fa fa-list-ul" aria-hidden="true"></i> Kết quả tìm kiếm : {{ $key}}</h2>
									@else
										<h2><i class="fa fa-list-ul" aria-hidden="true"></i> Các tin tức với tags : {{ $tags}}</h2>
									@endif
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                       {{-- {{dd($news_serch->count())}} --}}

                                <div class="tab-pane fade show active" id="world-tab-1" role="tabpanel" aria-labelledby="tab1">
                                    <!-- Single Blog Post -->
                                    <?php $i=0; ?>
                                 	@foreach($news_serch as $item_serch)
                                    <div class="single-blog-post post-style-4 d-flex align-items-center">
                                        <!-- Post Thumbnail -->
                                        <div class="post-thumbnail">
                                            <img src="{{url('/public/img/news/100x100/'.$item_serch->newimg)}}" alt="no img">
                                        </div>
                                        <!-- Post Content -->
                                        <div class="post-content">
                                            <a href="{{url('chi-tiet/'.$item_serch->slug)}}" class="headline" title="">
                                                <h5>{{$item_serch->newsname}}</h5>
                                            </a>
                                            <p>{{$item_serch->newintro}}</p>
                                            <!-- Post Meta -->
                                            <span class="item-meta">
												<a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$item_serch->created_at}}</a>
												<a href="{{url('/loai-tin/'.$item_serch->list_name($item_serch->idlistnew)['slug'])}}">
													<i class="fa fa-list-ul" aria-hidden="true"></i> 
													@if($item_serch->list_name($item_serch->idlistnew)['listname'] !="")
														{{ $item_serch->list_name($item_serch->idlistnew)['listname'] }}
													@else
														{{$item_serch->mod_name($item_serch->idmodnew)['modname']}}
													@endif
												</a>
											</span>
                                            
                                        </div>
                                    </div>
                                    @endforeach

                                   
                                </div>

                            
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ========== Sidebar Area ========== -->
                <div class="col-12 col-md-8 col-lg-4">
                    <div class="post-sidebar-area">
                        <!-- Widget Area -->
                        <div class="sidebar-widget-area">
                            <h5 class="title">Kết nối với chúng tôi</h5>
                            <div class="widget-content">
                                <p><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgoclamdepcungnang%2F&tabs&width=340&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=439880526654528" width="340" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe></p>
                            </div>
                        </div>
                        <!-- Widget Area -->
                       
                        <div class="sidebar-widget-area">
                            <h5 class="title">Đọc nhiều nhất</h5>
                            <div class="widget-content">
                                <!-- Single Blog Post -->
                                <?php $count =1; ?>
								@foreach($most_news as $item_most)
									@if($count <9)
                                <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                    <!-- Post Thumbnail -->
                                    <div class="post-thumbnail">
                                        <img src="{{url('/public/img/news/100x100/'.$item_most->newimg)}}" alt="">
                                    </div>
                                    <!-- Post Content -->
                                    <div class="post-content">
                                        <a href="{{url('chi-tiet/'.$item_most->slug)}}" class="headline">
                                            <h5 class="mb-0">{{$item_most->newsname}}</h5>
                                        </a>
                                    </div>
                                </div>
                                @endif
								<?php  $count = $count +1; ?>
								@endforeach
                               
                            </div>
                        </div>
                        <!-- Widget Area -->
                        <div class="sidebar-widget-area">
                            <h5 class="title">Hợp tác</h5>
                            <div class="widget-content">
                                <div class="social-area d-flex justify-content-between">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                    <a href="#"><i class="fa fa-vimeo"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Widget Area -->
                
                    </div>
                </div>
            </div>

           
        </div>
    </div>
@endsection