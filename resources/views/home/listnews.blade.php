@extends('home.master')
@section('title', (!empty($contact)?$contact->seo_title:""))
@section('seo_keyword', (!empty($contact)?$contact->seo_keyword:""))
@section('seo_description', (!empty($contact)?$contact->seo_description:""))
@section('seo_image', (!empty($contact)?asset($contact->seo_image):""))
@section('seo_url', url()->current())
@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="">Trang chủ</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $listnew->listname }}</li>
  </ol>
</nav>
</div>

     <div class="main-content-wrapper section-padding-100">
        <div class="container">
            <div class="row justify-content-center">
                <!-- ============= Post Content Area Start ============= -->
                <div class="col-12 col-lg-8">
                    <div class="post-content-area mb-100">
                        <!-- Catagory Area -->
                        <div class="world-catagory-area">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="title"><a href="{{ url('loai-tin/'.$listnew->slug) }}"><h2><i class="fa fa-list-ul" aria-hidden="true"></i> {{ $listnew->listname }}</h2></a></li>
                            </ul>

                            <div class="tab-content" id="myTabContent">

                                <div class="tab-pane fade show active" id="world-tab-1" role="tabpanel" aria-labelledby="tab1">
                                    <!-- Single Blog Post -->
                                    @foreach($listnews_cat as $item)
                                    <div class="single-blog-post post-style-4 d-flex align-items-center">
                                        <!-- Post Thumbnail -->
                                        <div class="post-thumbnail">
                                            <img src="{{url('public/img/news/300x300/'.$item->newimg)}}" alt="{{$item->newimg}}">
                                        </div>
                                        <!-- Post Content -->
                                        <div class="post-content">
                                            <a href="{{url('/chi-tiet/'.$item->slug)}}" class="headline" title="{{$item->newsname}}">
                                                <h5>{{$item->newsname}}</h5>
                                            </a>
                                             <p>{{$item->newintro}}</p>
                                            <!-- Post Meta -->
                                            <span class="item-meta">
												<a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$item->created_at}}</a>
												
											</span>
                                            
                                        </div>
                                    </div>
                                    @endforeach

                                   
                                </div>

                            
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ========== Sidebar Area ========== -->
                <div class="col-12 col-md-8 col-lg-4">
                    <div class="post-sidebar-area">
                        <!-- Widget Area -->
                      
                        <!-- Widget Area -->
                       
                        <div class="sidebar-widget-area">
                            <h5 class="title">Đọc nhiều nhất</h5>
                            <div class="widget-content">
                                <!-- Single Blog Post -->
                                <?php $count =1; ?>
								@foreach($most_news as $item_most)
									@if($count <9)
                                <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                    <!-- Post Thumbnail -->
                                    <div class="post-thumbnail">
                                        <img src="{{url('/public/img/news/100x100/'.$item_most->newimg)}}" alt="">
                                    </div>
                                    <!-- Post Content -->
                                    <div class="post-content">
                                        <a href="{{url('chi-tiet/'.$item_most->slug)}}" class="headline">
                                            <h5 class="mb-0">{{$item_most->newsname}}</h5>
                                        </a>
                                    </div>
                                </div>
                                @endif
								<?php  $count = $count +1; ?>
								@endforeach
                               
                            </div>
                        </div>
                        <!-- Widget Area -->
                        <div class="sidebar-widget-area">
                            <h5 class="title">Hợp tác</h5>
                            <div class="widget-content">
                                <div class="social-area d-flex justify-content-between">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                    <a href="#"><i class="fa fa-vimeo"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Widget Area -->
                    
                    </div>
                </div>
            </div>

           
        </div>
    </div>
@endsection