@extends('home.master')
@section('title', (!empty($contact)?$contact->seo_title:"Tin Tức Plus"))
@section('seo_keyword', (!empty($contact)?$contact->seo_keyword:"tin tức plus"))
@section('seo_description', (!empty($contact)?$contact->seo_description:"tin tức mới thời sự, sự kiện, văn hóa, thể thao, âm nhạc trong nước và quốc tế"))
@section('seo_image', (!empty($contact)?asset($contact->seo_image):"tin tức plus"))
@section('seo_url', url()->current())
@section('content')
<div class="container">
  <div class="ot-breaking-news-body" data-breaking-timeout="4000" data-breaking-autostart="true">
            <div class="ot-breaking-news-controls">
                <button class="right" data-break-dir="right"><i class="fa fa-angle-right"></i></button>
                <button class="right" data-break-dir="left"><i class="fa fa-angle-left"></i></button>
                <strong><i class="fa fa-bar-chart"></i>Sự kiện nóng  </strong>
            </div>
           
            <div class="hero-post-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="hero-post-slide">
                            <!-- Single Slide -->
                            @foreach($khuyenmai as $item_km)
                            <div class="single-slide d-flex align-items-center">
                               
                                <div class="post-title">
                                    <a href="{{url('/chi-tiet/'.$item_km->slug)}}">{{ $item_km->newsname}}</a>
                                </div>
                            </div>
                           @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
          
        <!-- END .ot-breaking-news-body -->
        </div>
</nav>
</div>

    <div class="main-content-wrapper section-padding-100">
        <div class="container">
            <div class="row justify-content-center">
                <!-- ============= Post Content Area Start ============= -->
                <div class="col-12 col-lg-8">
                    <div class="post-content-area mb-50">
                        <!-- Catagory Area -->
                        <div class="world-catagory-area">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="title">TIN MỚI</li>
                               
                            

                            </ul>

                            <div class="tab-content" id="myTabContent">

                           

                                <div class="tab-pane fade show active" id="world-tab-2" role="tabpanel" aria-labelledby="tab2">
                                	
                                    <div class="row">
                                       
                                        <div class="col-12 col-md-6">
                                            <!-- Single Blog Post -->
                                             @foreach($tinmoi_thethao as $tm)
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('public/img/news/800x800/'.$tm->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="{{url('/chi-tiet/'.$tm->slug)}}" class="headline">
                                                        <h5>{{ $tm -> newsname}}</h5>
                                                    </a>
                                                    <p></p>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                     <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tm->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        

                                        <div class="col-12 col-md-6">
                                            <!-- Single Blog Post -->
                                           @foreach($tinmoi_thethao1 as $tm1)
                                            <div class="single-blog-post post-style-2 d-flex align-items-center">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('public/img/news/300x300/'.$tm1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="{{url('/chi-tiet/'.$tm1->slug)}}" class="headline">
                                                        <h5>{{ $tm1 -> newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tm1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                             @endforeach

                                    
                                        </div>
                                    </div>
                                   
                                </div>

                  
                            </div>
                        </div>

                        <!-- Catagory Area -->
                     
                     <div class="world-latest-articles">
                <div class="row">
                    <div class="col-12 col-lg-12">
                        <div class="title">
                            <h5>TIN NỔI BẬT</h5>
                        </div>
                        @foreach($noibat as $nb)
                        <!-- Single Blog Post -->
                        <div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.2s">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <img src="{{url('/public/img/news/300x300/'.$nb->newimg)}}" alt="">
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="{{url('chi-tiet/'.$nb->slug)}}" class="headline">
                                    <h5>{{$nb->newsname}}</h5>
                                </a>
                                <p>{{$nb->newintro}}</p>
                                <!-- Post Meta -->
                                <div class="post-meta">
                                   <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$nb->created_at}}</a>
                                                    </span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                       
                    </div>

                 
                </div>
            </div>

        
                      


                    </div>
                </div>

                <!-- ========== Sidebar Area ========== -->
                <div class="col-12 col-md-8 col-lg-4">
                    <div class="post-sidebar-area wow fadeInUpBig" data-wow-delay="0.2s">
                        <!-- Widget Area -->
                         <div class="sidebar-widget-area">
                            <h5 class="title">Kết nối với chúng tôi</h5>
                            <div class="widget-content">
                                <p><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTin-t%25E1%25BB%25A9c-Plus-103488104765914&tabs&width=340&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe></p>
                            </div>
                        </div>
                        <!-- Widget Area -->
                       <div class="sidebar-widget-area">
                            <h5 class="title">Đọc nhiều nhất</h5>
                            <div class="widget-content">
                                <!-- Single Blog Post -->
                                <?php $count =1; ?>
                                @foreach($most_news as $item_most)
                                    @if($count <9)
                                <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                    <!-- Post Thumbnail -->
                                    <div class="post-thumbnail">
                                        <img src="{{url('/public/img/news/100x100/'.$item_most->newimg)}}" alt="">
                                    </div>
                                    <!-- Post Content -->
                                    <div class="post-content">
                                        <a href="{{url('chi-tiet/'.$item_most->slug)}}" class="headline">
                                            <h5 class="mb-0">{{$item_most->newsname}}</h5>
                                        </a>
                                    </div>
                                </div>
                                @endif
                                <?php  $count = $count +1; ?>
                                @endforeach
                               
                            </div>
                        </div>
                        <!-- Widget Area -->
                        <div class="sidebar-widget-area">
                            <h5 class="title">Hợp tác chúng tôi</h5>
                            <div class="widget-content">
                                <div class="social-area d-flex justify-content-between">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                    <a href="#"><i class="fa fa-vimeo"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Widget Area -->
                      
                    </div>
                </div>
            </div>

    <div class="row justify-content-center">
                <!-- ========== Single Blog Post ========== -->
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="single-blog-post post-style-3 mt-50 wow fadeInUpBig" data-wow-delay="0.2s">
                        <!-- Post Thumbnail -->
                      
                            <img src="public/home/img/blog-img/ba1.jpg" alt="">
                            <!-- Post Content -->
                          
                      
                    </div>
                </div>
                  <div class="container-fluid bg-dark mt-5">
                   <div class="row justify-content-center">
                <!-- ============= Post Content Area Start ============= -->
                <div class="col-12 col-lg-12">
                    <div class="post-content-area mb-50">
                        <!-- Catagory Area -->
                        <div class="world-catagory-area">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="title" style="color: white;">VIDEO NỔI BẬT</li>

                               
                            </ul>

                            <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="world-tab-1" role="tabpanel" aria-labelledby="tab1">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                            <div class="world-catagory-slider owl-carousel wow fadeInUpBig" data-wow-delay="0.1s">

                                                <!-- Single Blog Post -->
                                                @foreach($video_nb as $vnb)
                        <div class="single-blog-post wow fadeInUpBig" data-wow-delay="0.2s">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail bg-light">
                                <img src="{{url('/public/img/news/300x300/'.$vnb->newimg)}}" alt="">
                                <!-- Catagory -->
                               
                                <!-- Video Button -->
                                <a href="{{$vnb -> link}}" class="video-btn"><i class="fa fa-play" style="color: red;"></i></a>
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="#" class="headline" style="color: white">
                                    <h5>{{$vnb->newname}}</h5>
                                </a>
                             
                                <!-- Post Meta -->
                                 <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$vnb->created_at}}</a>
                                                    </span>
                                                    </div>
                            </div>
                        </div>
                        @endforeach

                                                <!-- Single Blog Post -->
                   
                                                <!-- Single Blog Post -->
                                        
                                            </div>
                                        </div>

                        <div class="col-12 col-md-6">
                                            <!-- Single Blog Post -->
                                             @foreach($video as $vi)
                        <div class="single-blog-post wow fadeInUpBig post-style-2 d-flex align-items-center" data-wow-delay="0.2s">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <img src="{{url('/public/img/news/300x300/'.$vi->newimg)}}" alt="">
                                <!-- Catagory -->
                               
                                <!-- Video Button -->
                                <a href="{{$vi->link}}" class="video-btn"><i class="fa fa-play" style="color: red;"></i></a>
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="#" class="headline" style="color: white">
                                    <h5>{{$vi->newname}}</h5>
                                </a>
                              
                                <!-- Post Meta -->
                                 <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$vi->created_at}}</a>
                                                    </span>
                                                    </div>
                            </div>
                        </div>
                         @endforeach


                        

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>
                </div>

            </div>
              </div>
               
            </div>

              <div class="row">
                       <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">THỜI SỰ</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab10" data-toggle="tab" href="#world-tab-10" role="tab" aria-controls="world-tab-10" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab11" data-toggle="tab" href="#world-tab-11" role="tab" aria-controls="world-tab-11" aria-selected="false">Đô thị</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab12" data-toggle="tab" href="#world-tab-12" role="tab" aria-controls="world-tab-12" aria-selected="false">Giao thông</a>
                                </li>

                            

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-10" role="tabpanel" aria-labelledby="tab10">
                                     <div class="row">

                                @foreach($thoisu as $ts)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$ts->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$ts->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$ts->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($thoisu_1 as $ts1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/100x100/'.$ts1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$ts1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$ts1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                        <div class="tab-pane fade" id="world-tab-11" role="tabpanel" aria-labelledby="tab11">
                                     <div class="row">

                                @foreach($dothi as $dt)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$dt->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$dt->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$dt->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($dothi_1 as $dt1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/100x100/'.$dt1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$dt1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$dt1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                      <div class="tab-pane fade" id="world-tab-12" role="tabpanel" aria-labelledby="tab12">
                                     <div class="row">

                                @foreach($giaothong as $gt)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$gt->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$gt->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                       <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$gt->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($giaothong_1 as $gt1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/100x100/'.$gt1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$gt1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$gt1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                        <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">THỂ THAO</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab13" data-toggle="tab" href="#world-tab-13" role="tab" aria-controls="world-tab-13" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab14" data-toggle="tab" href="#world-tab-14" role="tab" aria-controls="world-tab-14" aria-selected="false">Việt nam</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab15" data-toggle="tab" href="#world-tab-15" role="tab" aria-controls="world-tab-15" aria-selected="false">Cúp châu âu</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab16" data-toggle="tab" href="#world-tab-16" role="tab" aria-controls="world-tab-16" aria-selected="false">Thế giới</a>
                                </li>

                               

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-13" role="tabpanel" aria-labelledby="tab13">
                                     <div class="row">

                                @foreach($thethao as $tt)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$tt->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tt->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tt->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($thethao_1 as $tt1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/100x100/'.$tt1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tt1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tt1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="world-tab-15" role="tabpanel" aria-labelledby="tab15">
                                   <div class="row">

                                @foreach($chauau as $au)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$au->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$au->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$au->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($chauau_1 as $au1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/100x100/'.$au1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$au1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$au1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                  <div class="tab-pane fade" id="world-tab-14" role="tabpanel" aria-labelledby="tab14">
                                   <div class="row">

                                @foreach($vietnam as $vn)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$vn->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$vn->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$vn->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($vietnam_1 as $vn1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/100x100/'.$vn1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$vn1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$vn1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                   <div class="tab-pane fade" id="world-tab-16" role="tabpanel" aria-labelledby="tab16">
                                   <div class="row">

                                @foreach($tt_thegioi as $tg)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$tg->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tg->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tg->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($tt_thegioi_1 as $tg1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$tg1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tg1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tg1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-17" role="tabpanel" aria-labelledby="tab17">
                                   <div class="row">

                                @foreach($bongda as $bd)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$bd->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$bd->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$bd->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($bongda_1 as $bd1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$bd1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$bd1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$bd1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>


                            
                            </div>
                        </div>
                           <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">GÍAO DỤC</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab18" data-toggle="tab" href="#world-tab-18" role="tab" aria-controls="world-tab-18" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab19" data-toggle="tab" href="#world-tab-19" role="tab" aria-controls="world-tab-19" aria-selected="false">Tuyển sinh</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab20" data-toggle="tab" href="#world-tab-20" role="tab" aria-controls="world-tab-20" aria-selected="false">Tư vấn</a>
                                </li>

                              

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-18" role="tabpanel" aria-labelledby="18">
                                     <div class="row">

                                @foreach($giaoduc as $gd)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$gd->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$gd->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$gd->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($giaoduc_1 as $gd1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$gd1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$gd1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$gd1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="world-tab-19" role="tabpanel" aria-labelledby="tab19">
                                    <div class="row">

                                @foreach($tuyensinh as $tsi)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$tsi->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tsi->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tsi->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($tuyensinh_1 as $tsi1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$tsi1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tsi1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tsi1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                 <div class="tab-pane fade" id="world-tab-20" role="tabpanel" aria-labelledby="tab20">
                                    <div class="row">

                                @foreach($tuvan as $tv)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$tv->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tv->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tv->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($tuvan_1 as $tv1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$tv1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tv1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tv1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                           <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">CÔNG NGHỆ</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab21" data-toggle="tab" href="#world-tab-21" role="tab" aria-controls="world-tab-21" aria-selected="true">Tất cả</a>
                                </li>

                              
                                <li class="nav-item">
                                    <a class="nav-link" id="tab22" data-toggle="tab" href="#world-tab-22" role="tab" aria-controls="world-tab-22" aria-selected="false">Điện thoại</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab23" data-toggle="tab" href="#world-tab-23" role="tab" aria-controls="world-tab-23" aria-selected="false">Ứng dụng di động</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab24" data-toggle="tab" href="#world-tab-24" role="tab" aria-controls="world-tab-24" aria-selected="false">Máy tính</a>
                                </li>

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-21" role="tabpanel" aria-labelledby="tab21">
                                   <div class="row">

                                @foreach($congnghe as $cn)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$cn->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$cn->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$cn->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($congnghe_1 as $cn1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$cn1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$cn1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$cn1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="world-tab-22" role="tabpanel" aria-labelledby="tab22">
                                    <div class="row">

                                @foreach($dienthoai as $dt)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$dt->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$dt->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$dt->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($dienthoai_1 as $gd1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$dt1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$dt1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$dt1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                       <div class="tab-pane fade" id="world-tab-23" role="tabpanel" aria-labelledby="tab23">
                                    <div class="row">

                                @foreach($didong as $dd)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$dd->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$dd->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$dd->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($didong_1 as $dd1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$dd1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$dd1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$dd1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                      <div class="tab-pane fade" id="world-tab-24" role="tabpanel" aria-labelledby="tab24">
                                    <div class="row">

                                @foreach($maytinh as $mt)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$mt->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$mt->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$mt->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($maytinh_1 as $mt1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$mt1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$mt1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$mt1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                           <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">ÂM NHẠC</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab25" data-toggle="tab" href="#world-tab-25" role="tab" aria-controls="world-tab-25" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab26" data-toggle="tab" href="#world-tab-26" role="tab" aria-controls="world-tab-26" aria-selected="false">Nhạc âu mỹ</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab27" data-toggle="tab" href="#world-tab-27" role="tab" aria-controls="world-tab-27" aria-selected="false">Nhạc hàn</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab28" data-toggle="tab" href="#world-tab-28" role="tab" aria-controls="world-tab-28" aria-selected="false">Nhạc việt</a>
                                </li>

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-25" role="tabpanel" aria-labelledby="tab25">
                                         
                                    <div class="row">

                                @foreach($amnhac as $am)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$am->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$am->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$am->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($amnhac_1 as $am1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$am1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$am1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$am1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    
                                </div>
                                </div>

                                <div class="tab-pane fade" id="world-tab-26" role="tabpanel" aria-labelledby="tab26">
                                     <div class="row">

                                @foreach($nhacmy as $nm)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$nm->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$nm->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$nm->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($nhacmy_1 as $nm1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$nm1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$nm1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$nm1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-27" role="tabpanel" aria-labelledby="tab27">
                                     <div class="row">

                                @foreach($nhachan as $nh)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$nh->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$nh->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$nh->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($nhachan_1 as $nh1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$nh1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$nh1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$nh1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-28" role="tabpanel" aria-labelledby="tab28">
                                     <div class="row">

                                @foreach($nhacviet as $nv)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$nv->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$nv->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$nv->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($nhacviet_1 as $nv1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$nv1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$nv1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$nv1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                           <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">KINH DOANH</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab29" data-toggle="tab" href="#world-tab-29" role="tab" aria-controls="world-tab-29" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab30" data-toggle="tab" href="#world-tab-30" role="tab" aria-controls="world-tab-30" aria-selected="false">Tài chính</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab31" data-toggle="tab" href="#world-tab-31" role="tab" aria-controls="world-tab-31" aria-selected="false">Bất động sản</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab32" data-toggle="tab" href="#world-tab-32" role="tab" aria-controls="world-tab-32" aria-selected="false">Chứng khoáng</a>
                                </li>

                               
                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                     <div class="tab-pane fade show active" id="world-tab-29" role="tabpanel" aria-labelledby="tab29">
                                     <div class="row">

                                @foreach($kinhdoanh as $kd)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$kd->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$kd->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$kd->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($kinhdoanh_1 as $kd1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$kd1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$kd1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$kd1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                                   <div class="tab-pane fade" id="world-tab-30" role="tabpanel" aria-labelledby="tab30">
                                     <div class="row">

                                @foreach($taichinh as $tc)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$tc->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tc->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tc->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($taichinh_1 as $tc1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$tc1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$tc1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$tc1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-31" role="tabpanel" aria-labelledby="tab31">
                                     <div class="row">

                                @foreach($bds as $bds)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$bds->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$bds->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$bds->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($bds_1 as $bds1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$bds1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$bds1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$bds1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-32" role="tabpanel" aria-labelledby="tab32">
                                     <div class="row">

                                @foreach($chungkhoang as $ck)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/800x800/'.$ck->newimg)}}" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$ck->newsname}}</h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$ck->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                             @foreach($chungkhoang_1 as $ck1)
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="{{url('/public/img/news/300x300/'.$ck1->newimg)}}" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5>{{$ck1->newsname}}</h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> {{$ck1->created_at}}</a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      @endforeach

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                        </div>

        
         
            
  

        </div>
    </div>
@endsection  