@extends('home.master')
@section('title', (!empty($contact)?$contact->seo_title:""))
@section('seo_keyword', (!empty($contact)?$contact->seo_keyword:""))
@section('seo_description', (!empty($contact)?$contact->seo_description:""))
@section('seo_image', (!empty($contact)?asset($contact->seo_image):""))
@section('seo_url', url()->current())
@section('content')

<div class="container">
    <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="">Trang chủ</a></li>
    <li class="breadcrumb-item"><a href="#">@if($itemnews->idlistnew !="")
                            <a href="{{url('loai-tin/'.$itemnews->list_name($itemnews->idlistnew)['slug'])}}">{{ $itemnews->list_name($itemnews->idlistnew)['listname'] }}</a>
                        @elseif($itemnews->idmodnew !="")
                            Danh mục : <a href="{{url('loai-tin/'.$itemnews->mod_name($itemnews->idmodnew)['slug'])}}">{{ $itemnews->mod_name($itemnews->idmodnew)['modname'] }}</a>
                        @endif</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{$itemnews->newsname}}</li>
  </ol>
</nav>
</div>


	<div class="main-content-wrapper section-padding-100">
        <div class="container">
            <div class="row justify-content-center">
                <!-- ============= Post Content Area ============= -->
                <div class="col-12 col-lg-8">
                    <div class="single-blog-content mb-100">
                        <!-- Post Meta -->
                       
                        <style type="text/css">
            .cc1{
                width: 200px;
                height: 200px;
                position:fixed;
                left: 11%;
                top: 40%;
                display: none;
                z-index: 1000;
            }
            .cc1 i{
                font-size: 40px;
            }
            .cc1 i:hover{
                padding-left: 7px;
            }
            .fa-facebook-square{
                color:blue;
            }
            .fa-youtube-play{
                color: red;
            }
            .fa-twitter-square{
                color: #51A9FF;
            }
            .fa-google-plus-square{
                color: #FF5900;
            }
        </style>
        <div class="cc1">
            <div><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></div>
            <div><a href=""><i class="fa fa-youtube-square"></i></a></div>
            <div><a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a></div>
            <div><a href=""><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></div>
        </div>
       
				
                        <!-- Post Content -->
                        <div class="post-content pb-5">
                        	<strong class="category-link">
						@if($itemnews->idlistnew !="")
							Danh mục : <a href="{{url('loai-tin/'.$itemnews->list_name($itemnews->idlistnew)['slug'])}}"  style="color: blue;">{{ $itemnews->list_name($itemnews->idlistnew)['listname'] }}</a>
						@elseif($itemnews->idmodnew !="")
							Danh mục : <a href="{{url('loai-tin/'.$itemnews->mod_name($itemnews->idmodnew)['slug'])}}"  style="color: blue;">{{ $itemnews->mod_name($itemnews->idmodnew)['modname'] }}</a>
						@endif
						</strong>
                        	<h2>{{$itemnews->newsname}}</h2>
                        		
                           <div class="content-panel-body article-main-share" style="background-color: #e9ecef;padding: 5px 0">
                              
                              
                               <div class="fb-share-button pl-2" data-href="{{url()->current()}}" data-layout="button_count" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                                 <!-- Your like button code -->
  <div class="fb-like" data-href="{{url()->current()}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div> 
  
                                  
                               <span>|</span>
                           
                                 <a href="#" title="" class="meta-item" style="color: #6c757d;"><i class="fa fa-eye"></i> {{ $itemnews->view_count }} – </a>  
                            <a href="#comments" class="meta-item" style="color: #6c757d;"><i class="fa fa-comment"></i> <span class="fb-comments-count" data-href="{{url()->current()}}"></span> – </a>
                             <a href="#" class="post-author"><a href="#" class="meta-item" style="color: #6c757d;"><i class="fa fa-clock-o"></i> {{ $itemnews->created_at }}  </a>       </a>
                            
                                        
                            </div>
                            <hr>
                     
                            <h6 class="pb-5">	{!! $itemnews->newcontent !!}</h6>
                            Tác giả: <strong>{{$itemnews->newuser}}</strong>
                            <div class="pt-3 pb-3">
                            	  
                               <div class="fb-share-button" data-href="{{url()->current()}}" data-layout="button_count" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
                                 <!-- Your like button code -->
  <div class="fb-like" data-href="{{url()->current()}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div> 
                            </div>
                          		<!-- BEGIN .content-panel -->


				<!-- BEGIN .content-panel -->
				<div class="post-meta">
					<div class="content-panel-body article-main-tags">
						<span class="tags-front"><i class="fa fa-tags"></i>Tags</span><br/>
					<?php 
						if($itemnews->newtag !=""){
							$tags = explode(", ", $itemnews->newtag);
						}
					?>
					@if(!empty($tags))
						@for($count=0; $count < count($tags);$count ++ )
							<a href="{{url('/tags/'.$tags[$count])}}">{{$tags[$count]}}</a>
						@endfor
					@endif
					</div>
				<!-- END .content-panel -->
				</div>
                         
                            <!-- Post Tags -->
                            <div class="post-meta">
               <div class="content-panel-body article-main-tags">
                                <ul style="background-color: #e9ecef; padding: 5px 5px;">
                                     <strong>TIN LIÊN QUAN</strong>
                                      <?php $count =1; ?>
                                      @foreach($most_news as $item_most)
                                       @if($count <4)
                                    <li><a href="{{url('chi-tiet/'.$item_most->slug)}}"><strong>+</strong> {{$item_most->newsname}}</a></li>
                                     @endif
                                <?php  $count = $count +1; ?>
                                    @endforeach
                                    
                                </ul>
                            </div>
             </div>
                            <!-- Post Meta -->
                           
                        </div>
                    </div>
                </div>




                <!-- ========== Sidebar Area ========== -->
                <div class="col-12 col-md-8 col-lg-4">
                    <div class="post-sidebar-area mb-100">
                        <!-- Widget Area -->
                       
                        <!-- Widget Area -->
                        <div class="sidebar-widget-area">
                            <h5 class="title">Đọc nhiều nhất</h5>
                            <div class="widget-content">
                                <!-- Single Blog Post -->
                                <?php $count =1; ?>
                                @foreach($most_news as $item_most)
                                    @if($count <9)
                                <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                    <!-- Post Thumbnail -->
                                    <div class="post-thumbnail">
                                        <img src="{{url('/public/img/news/100x100/'.$item_most->newimg)}}" alt="">
                                    </div>
                                    <!-- Post Content -->
                                    <div class="post-content">
                                        <a href="{{url('chi-tiet/'.$item_most->slug)}}" class="headline">
                                            <h5 class="mb-0">{{$item_most->newsname}}</h5>
                                        </a>
                                    </div>
                                </div>
                                @endif
                                <?php  $count = $count +1; ?>
                                @endforeach
                               
                            </div>
                        </div>
                        <!-- Widget Area -->
                        <div class="sidebar-widget-area">
                            <h5 class="title">Hợp tác</h5>
                            <div class="widget-content">
                                <div class="social-area d-flex justify-content-between">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                    <a href="#"><i class="fa fa-vimeo"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                  
                    </div>
                </div>
            </div>

            <!-- ============== Related Post ============== -->
              <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="post-a-comment-area mt-70">
                       <div class="content-panel">
                    <div class="content-panel-title">
                        <h2> Ý kiến của bạn</h2>
                    </div>
                    <div class="content-panel-body comment-list">                       
                        <div class="fb-comments" data-href="{{url()->current()}}" data-width="100%" data-numposts="5"></div>
                    </div>
                <!-- END .content-panel -->
                </div>
                    </div>
                </div>

               
            </div>

             @if($new_in_list_active->count()>0)
               <div class="content-panel-title mt-5">
                        <h4> Đọc tiếp</h4>
                        <hr>
                    </div>
            <div class="row">
                
                @foreach($new_in_list_active as $item_lt )
                <div class="col-12 col-md-6 col-lg-4">
                    <!-- Single Blog Post -->
                    <div class="single-blog-post">
                        <!-- Post Thumbnail -->
                        <div class="post-thumbnail">
                            <img src="{{url('/public/img/news/800x800/'.$item_lt->newimg)}}" alt="">
                            <!-- Catagory -->
                           
                        </div>
                        <!-- Post Content -->
                        <div class="post-content">
                            <a href="{{url('chi-tiet/'.$item_lt->slug)}}" class="headline">
                                <h5>{{$item_lt->newsname}}</h5>
                            </a>
                            <p>{{$item_lt->newintro}}</p>
                            <!-- Post Meta -->
                            
                        </div>
                    </div>
                </div>
                 @endforeach
                    @endif



   

            </div>

          
        </div>
    </div>
@endsection