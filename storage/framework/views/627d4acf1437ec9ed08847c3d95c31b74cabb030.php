<?php $__env->startSection('title','Videos'); ?>
<?php $__env->startSection('content'); ?>
<div id="page-wrapper">
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Videos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <button type="button" class="btn btn-info" data-toggle="modal" data-target=".nn-modal-add-news" id="nn-add-job">+ Thêm Videos</button>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        <?php if(session('thongbao')): ?>
                            <div class="alert-tb alert alert-success">
                                <span class="fa fa-check"> </span> <?php echo e(session('thongbao')); ?>

                            </div>
                        <?php endif; ?>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tên bài viết</th>                                  
                                        <th>Trạng thái</th>
                                        <th>Hình ảnh</th>
                                        <th>Link</th>
                                        <th>Chi tiết</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $__currentLoopData = $lnews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="odd gradeX info">
                                        <td><?php echo e($new->id); ?></td>
                                        <td><?php echo e($new->newname); ?></td>
                                        <td>
                                            <?php if($new->status ==2): ?>
                                                <strong class="text-success">Nổi bật</strong>
                                            <?php elseif($new->status ==1): ?>
                                                <strong class="text-info">Đang Hiện</strong>
                                            <?php else: ?>
                                                <strong class="text-danger">Đang Ẩn</strong>
                                            <?php endif; ?>
                                        </td>
                                        <td class="center">
                                            <img src="<?php echo e(asset('public/img/news/300x300/'.$new->newimg)); ?>" style="width: 55px"> 
                                        </td>
                                        <td>
                                            <p><?php echo e($new->link); ?></p>
                                        </td>
                                                                       
                                       <td>
                                            <i class="nneditnew btn btn-info fa fa-edit" id="ennnew<?php echo e($new->id); ?>"  
                                                    editid="<?php echo e($new->id); ?>" name="<?php echo e($new->newsname); ?>" imgo="<?php echo e($new->newimg); ?>" newvideo="<?php echo e($new->newvideo); ?>" > Sửa</i>
                                            <i class="nndeditnew btn btn-danger fa fa-trash" imgo="<?php echo e($new->newimg); ?>" editid="<?php echo e($new->id); ?>" name="<?php echo e($new->newname); ?>"> Xóa </i>
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
</div>
<!-- model -->

<div class="modal fade nn-modal-add-news" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="margin-top: -3%;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Thêm Videos</h4>
          </div>
          <form class="form-horizontal" method="post" action="list" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" />
          <div class="modal-body">
            <div class="row">
                <?php if(count($errors)>0): ?>
                    <div class="alert-tb alert alert-danger">
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <i class="fa fa-exclamation-circle"></i> <?php echo e($err); ?><br/>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php endif; ?>
                <div class="col-xs-12 col-sm-12 col-md-12">
                   
                  
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <label for="newsname" class="control-label"><i class="fa  fa-newspaper-o"></i> Tiêu đề:</label>
                              <input type="text" class="form-control" name="newsname" id="nntitlenew" placeholder="tiêu đề bài viết" value="<?php echo old('newsname'); ?>">
                        </div>
                    </div>
                     <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <label for="nnavatar" class="control-label"><i class="fa  fa-picture-o"></i> Hình ảnh đại diện</label> <br>
                            <img id="nnavatar" src="https://shopproject30.com/wp-content/themes/venera/images/placeholder-camera-green.png" alt="..." class="img-thumbnail" style="width: 50%;">
                            <input type="file" name="nnavatarfile" id="nnavatarfile" onchange="showimg(this);">
                        </div>
                    </div>
                     <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="link" class="control-label"><i class="fa  fa-youtube"></i> Đường link</label>
                                <div class="col-xs-12 col-md-12">
                                  <input type="text" class="form-control" name="link" id="enntitlenew" placeholder="Link bài viết" value="<?php echo old('link'); ?>">
                                </div>
                            </div>
                        </div>
                  
                
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <div class="col-xs-12 col-md-12 text-center">
                                <label class="radio-inline">
                                    <input type="radio" name="nnhide" value="2" > Nổi bật 
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="nnhide" value="1" checked> Bình thường 
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="nnhide" value="0"> Ẩn 
                                </label>                                
                            </div>                    
                        </div>
                    </div>

          
                </div>
               
                 
              
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng cửa sổ</button>
            <button type="submit" class="btn btn-primary">Tạo mới</button>
          </div>
          </form>
        </div>
      </div>
    </div>
</div>
    <!-- end modal -->
<div class="modal fade nn-modal-edit-news" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="margin-top: -3%;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Sửa Videos</h4>
          </div>
          <form class="form-horizontal" method="post" action="list/edit" enctype="multipart/form-data">
          <input type="hidden" name="ennidnews" id="ennidnews" />
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" />
          <div class="modal-body">
            <div class="row">
                <?php if(count($errors)>0): ?>
                    <div class="alert-tb alert alert-danger">
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <i class="fa fa-exclamation-circle"></i> <?php echo e($err); ?><br/>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php endif; ?>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                       
                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="newsname" class="control-label"><i class="fa  fa-newspaper-o"></i> Tiêu đề:</label>
                                <div class="col-xs-12 col-md-12">
                                  <input type="text" class="form-control" name="newsname" id="enntitlenew" placeholder="Tiêu đề video" value="<?php echo old('newname'); ?>">
                                </div>
                            </div>
                        </div>
                         <div class="col-sx-12 col-md-12">
                            <div class="form-group">
                                <label for="ennavatar" class="control-label"><i class="fa  fa-picture-o"></i> Hình ảnh</label>
                                <div class="col-xs-12 col-md-12">
                                    <img id="ennavatar" src="http://shopproject30.com/wp-content/themes/venera/images/placeholder-camera-green.png" alt="..." class="img-thumbnail" style="width: 50%;">
                                    <input type="file" name="ennavatarfile" id="ennavatarfile" onchange="eshowimg(this);" style="display: none">
                                    <input type="hidden" name="ennimguserold" id="ennimguserold">
                                </div>
                            </div>
                        </div>
                          <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="link" class="control-label"><i class="fa  fa-newspaper-o"></i> Đường link</label>
                                <div class="col-xs-12 col-md-12">
                                  <input type="text" class="form-control" name="link" id="enntitlenew" placeholder="Link bài viết" value="<?php echo old('link'); ?>">
                                </div>
                            </div>
                        </div>
                         
                        <div class="col-sx-12 col-md-12">
                            <div class="form-group">
                                <label class="radio-inline">
                                    <input type="radio" name="ennhide" value="2" checked> Nổi bật 
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="ennhide" value="1" checked > Bình thường 
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="ennhide" value="0"> Ẩn 
                                </label>                  
                            </div>
                         </div>
                    </div>
                   
                       
                 
                   
                   
                </div>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng cửa sổ</button>
            <button type="submit" class="btn btn-primary">Cập nhật</button>
          </div>
          </form>
        </div>
      </div>
    </div>
</div>
    <!-- end modal -->
<div class="modal fade nn-modal-delete-news" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Xóa Tin</h4>
          </div>
          <form class="form-horizontal" method="post" action="list/delete" enctype="multipart/form-data">
          <input type="hidden" name="dennidnew" id="dennidnew" /> 
          <input type="hidden" name="dennimgnew" id="dennimgnew" /> 
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" />
          <div class="modal-body">
            <div class="row">
                <h4 class="nnbodydelete">Bạn có chắc xóa tin <i id="deletename"></i></h4>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">Đóng cửa sổ</button>
            <button type="submit" class="btn btn-warning">Xóa</button>
          </div>
          </form>
        </div>
      </div>
    </div>
</div>
    <!-- end modal -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
  <script src="<?php echo e(asset('public/js/admin/news.js')); ?>"></script>
  <script type="text/javascript">
    <?php if(session('actionuser')=='add' && count($errors) > 0): ?>
        $('.nn-modal-add-news').modal('show');
    <?php endif; ?>
    <?php if(session('actionuser')=='edit' && count($errors) > 0): ?>
        $(document).ready(function(){
          $("#ennnew<?php echo e(session('editid')); ?>").trigger('click');
        });
    <?php endif; ?>
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>