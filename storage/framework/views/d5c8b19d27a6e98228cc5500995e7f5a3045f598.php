<?php $__env->startSection('title', (!empty($contact)?$contact->seo_title:"")); ?>
<?php $__env->startSection('seo_keyword', (!empty($contact)?$contact->seo_keyword:"")); ?>
<?php $__env->startSection('seo_description', (!empty($contact)?$contact->seo_description:"")); ?>
<?php $__env->startSection('seo_image', (!empty($contact)?asset($contact->seo_image):"")); ?>
<?php $__env->startSection('seo_url', url()->current()); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
  <div class="ot-breaking-news-body" data-breaking-timeout="4000" data-breaking-autostart="true">
            <div class="ot-breaking-news-controls">
                <button class="right" data-break-dir="right"><i class="fa fa-angle-right"></i></button>
                <button class="right" data-break-dir="left"><i class="fa fa-angle-left"></i></button>
                <strong><i class="fa fa-bar-chart"></i>Tin mới  </strong>
            </div>
           
            <div class="hero-post-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="hero-post-slide">
                            <!-- Single Slide -->
                            <?php $__currentLoopData = $khuyenmai; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item_km): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="single-slide d-flex align-items-center">
                               
                                <div class="post-title">
                                    <a href="<?php echo e(url('/chi-tiet/'.$item_km->slug)); ?>"><?php echo e($item_km->newsname); ?></a>
                                </div>
                            </div>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          
        <!-- END .ot-breaking-news-body -->
        </div>
</nav>
</div>

    <div class="main-content-wrapper section-padding-100">
        <div class="container">
            <div class="row justify-content-center">
                <!-- ============= Post Content Area Start ============= -->
                <div class="col-12 col-lg-8">
                    <div class="post-content-area mb-50">
                        <!-- Catagory Area -->
                        <div class="world-catagory-area">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="title">TIN MỚI</li>
                               
                            

                            </ul>

                            <div class="tab-content" id="myTabContent">

                           

                                <div class="tab-pane fade show active" id="world-tab-2" role="tabpanel" aria-labelledby="tab2">
                                	
                                    <div class="row">
                                       
                                        <div class="col-12 col-md-6">
                                            <!-- Single Blog Post -->
                                             <?php $__currentLoopData = $tinmoi_thethao; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('public/img/news/800x800/'.$tm->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="<?php echo e(url('/chi-tiet/'.$tm->slug)); ?>" class="headline">
                                                        <h5><?php echo e($tm -> newsname); ?></h5>
                                                    </a>
                                                    <p></p>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                     <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tm->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                        

                                        <div class="col-12 col-md-6">
                                            <!-- Single Blog Post -->
                                           <?php $__currentLoopData = $tinmoi_thethao1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tm1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="single-blog-post post-style-2 d-flex align-items-center">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('public/img/news/300x300/'.$tm1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="<?php echo e(url('/chi-tiet/'.$tm1->slug)); ?>" class="headline">
                                                        <h5><?php echo e($tm1 -> newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tm1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    
                                        </div>
                                    </div>
                                   
                                </div>

                  
                            </div>
                        </div>

                        <!-- Catagory Area -->
                     
                     <div class="world-latest-articles">
                <div class="row">
                    <div class="col-12 col-lg-12">
                        <div class="title">
                            <h5>TIN NỔI BẬT</h5>
                        </div>
                        <?php $__currentLoopData = $noibat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <!-- Single Blog Post -->
                        <div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.2s">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <img src="<?php echo e(url('/public/img/news/300x300/'.$nb->newimg)); ?>" alt="">
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="<?php echo e(url('chi-tiet/'.$nb->slug)); ?>" class="headline">
                                    <h5><?php echo e($nb->newsname); ?></h5>
                                </a>
                                <p><?php echo e($nb->newintro); ?></p>
                                <!-- Post Meta -->
                                <div class="post-meta">
                                   <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($nb->created_at); ?></a>
                                                    </span>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       
                    </div>

                 
                </div>
            </div>

        
                      


                    </div>
                </div>

                <!-- ========== Sidebar Area ========== -->
                <div class="col-12 col-md-8 col-lg-4">
                    <div class="post-sidebar-area wow fadeInUpBig" data-wow-delay="0.2s">
                        <!-- Widget Area -->
                         <div class="sidebar-widget-area">
                            <h5 class="title">Kết nối với chúng tôi</h5>
                            <div class="widget-content">
                                <p><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgoclamdepcungnang%2F&tabs&width=340&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=439880526654528" width="340" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe></p>
                            </div>
                        </div>
                        <!-- Widget Area -->
                       <div class="sidebar-widget-area">
                            <h5 class="title">Đọc nhiều nhất</h5>
                            <div class="widget-content">
                                <!-- Single Blog Post -->
                                <?php $count =1; ?>
                                <?php $__currentLoopData = $most_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item_most): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($count <9): ?>
                                <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                    <!-- Post Thumbnail -->
                                    <div class="post-thumbnail">
                                        <img src="<?php echo e(url('/public/img/news/100x100/'.$item_most->newimg)); ?>" alt="">
                                    </div>
                                    <!-- Post Content -->
                                    <div class="post-content">
                                        <a href="<?php echo e(url('chi-tiet/'.$item_most->slug)); ?>" class="headline">
                                            <h5 class="mb-0"><?php echo e($item_most->newsname); ?></h5>
                                        </a>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <?php  $count = $count +1; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                               
                            </div>
                        </div>
                        <!-- Widget Area -->
                        <div class="sidebar-widget-area">
                            <h5 class="title">Hợp tác chúng tôi</h5>
                            <div class="widget-content">
                                <div class="social-area d-flex justify-content-between">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                    <a href="#"><i class="fa fa-vimeo"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Widget Area -->
                      
                    </div>
                </div>
            </div>

    <div class="row justify-content-center">
                <!-- ========== Single Blog Post ========== -->
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="single-blog-post post-style-3 mt-50 wow fadeInUpBig" data-wow-delay="0.2s">
                        <!-- Post Thumbnail -->
                      
                            <img src="public/home/img/blog-img/ba1.jpg" alt="">
                            <!-- Post Content -->
                          
                      
                    </div>
                </div>
                  <div class="container-fluid bg-dark mt-5">
                   <div class="row justify-content-center">
                <!-- ============= Post Content Area Start ============= -->
                <div class="col-12 col-lg-12">
                    <div class="post-content-area mb-50">
                        <!-- Catagory Area -->
                        <div class="world-catagory-area">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="title" style="color: white;">VIDEO NỔI BẬT</li>

                               
                            </ul>

                            <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="world-tab-1" role="tabpanel" aria-labelledby="tab1">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                            <div class="world-catagory-slider owl-carousel wow fadeInUpBig" data-wow-delay="0.1s">

                                                <!-- Single Blog Post -->
                                                <?php $__currentLoopData = $video_nb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vnb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="single-blog-post wow fadeInUpBig bg-light" data-wow-delay="0.2s">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <img src="<?php echo e(url('/public/img/news/300x300/'.$vnb->newimg)); ?>" alt="">
                                <!-- Catagory -->
                               
                                <!-- Video Button -->
                                <a href="https://www.youtube.com/watch?v=IhnqEwFSJRg" class="video-btn"><i class="fa fa-play" style="color: red;"></i></a>
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="#" class="headline">
                                    <h5><?php echo e($vnb->newname); ?></h5>
                                </a>
                             
                                <!-- Post Meta -->
                                 <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($vnb->created_at); ?></a>
                                                    </span>
                                                    </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                <!-- Single Blog Post -->
                   
                                                <!-- Single Blog Post -->
                                        
                                            </div>
                                        </div>

                        <div class="col-12 col-md-6">
                                            <!-- Single Blog Post -->
                                             <?php $__currentLoopData = $video; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="single-blog-post wow fadeInUpBig post-style-2 d-flex align-items-center bg-light" data-wow-delay="0.2s">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <img src="<?php echo e(url('/public/img/news/300x300/'.$vi->newimg)); ?>" alt="">
                                <!-- Catagory -->
                               
                                <!-- Video Button -->
                                <a href="<?php echo e($vi->link); ?>" class="video-btn"><i class="fa fa-play" style="color: red;"></i></a>
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="#" class="headline">
                                    <h5><?php echo e($vi->newname); ?></h5>
                                </a>
                              
                                <!-- Post Meta -->
                                 <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($vi->created_at); ?></a>
                                                    </span>
                                                    </div>
                            </div>
                        </div>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                        

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>



                    </div>
                </div>

            </div>
              </div>
               
            </div>

              <div class="row">
                       <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">THỜI SỰ</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab10" data-toggle="tab" href="#world-tab-10" role="tab" aria-controls="world-tab-10" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab11" data-toggle="tab" href="#world-tab-11" role="tab" aria-controls="world-tab-11" aria-selected="false">Đô thị</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab12" data-toggle="tab" href="#world-tab-12" role="tab" aria-controls="world-tab-12" aria-selected="false">Giao thông</a>
                                </li>

                            

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-10" role="tabpanel" aria-labelledby="tab10">
                                     <div class="row">

                                <?php $__currentLoopData = $thoisu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$ts->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($ts->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($ts->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $thoisu_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ts1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/100x100/'.$ts1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($ts1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($ts1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                        <div class="tab-pane fade" id="world-tab-11" role="tabpanel" aria-labelledby="tab11">
                                     <div class="row">

                                <?php $__currentLoopData = $dothi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$dt->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($dt->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($dt->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $dothi_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/100x100/'.$dt1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($dt1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($dt1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                      <div class="tab-pane fade" id="world-tab-12" role="tabpanel" aria-labelledby="tab12">
                                     <div class="row">

                                <?php $__currentLoopData = $giaothong; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$gt->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($gt->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                       <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($gt->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $giaothong_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gt1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/100x100/'.$gt1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($gt1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($gt1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                        <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">THỂ THAO</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab13" data-toggle="tab" href="#world-tab-13" role="tab" aria-controls="world-tab-13" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab14" data-toggle="tab" href="#world-tab-14" role="tab" aria-controls="world-tab-14" aria-selected="false">Việt nam</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab15" data-toggle="tab" href="#world-tab-15" role="tab" aria-controls="world-tab-15" aria-selected="false">Cúp châu âu</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab16" data-toggle="tab" href="#world-tab-16" role="tab" aria-controls="world-tab-16" aria-selected="false">Thế giới</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab17" data-toggle="tab" href="#world-tab-17" role="tab" aria-controls="world-tab-17" aria-selected="false">Bóng đá</a>
                                </li>

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-13" role="tabpanel" aria-labelledby="tab13">
                                     <div class="row">

                                <?php $__currentLoopData = $thethao; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$tt->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tt->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tt->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $thethao_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tt1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$tt1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tt1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tt1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="world-tab-15" role="tabpanel" aria-labelledby="tab15">
                                   <div class="row">

                                <?php $__currentLoopData = $chauau; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $au): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$au->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($au->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($au->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $chauau_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $au1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$au1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($au1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($au1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                  <div class="tab-pane fade" id="world-tab-14" role="tabpanel" aria-labelledby="tab14">
                                   <div class="row">

                                <?php $__currentLoopData = $vietnam; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$vn->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($vn->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($vn->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $vietnam_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vn1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$vn1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($vn1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($vn1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                   <div class="tab-pane fade" id="world-tab-16" role="tabpanel" aria-labelledby="tab16">
                                   <div class="row">

                                <?php $__currentLoopData = $tt_thegioi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$tg->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tg->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tg->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $tt_thegioi_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tg1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$tg1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tg1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tg1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-17" role="tabpanel" aria-labelledby="tab17">
                                   <div class="row">

                                <?php $__currentLoopData = $bongda; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$bd->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($bd->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($bd->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $bongda_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bd1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$bd1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($bd1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($bd1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>


                            
                            </div>
                        </div>
                           <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">GÍAO DỤC</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab18" data-toggle="tab" href="#world-tab-18" role="tab" aria-controls="world-tab-18" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab19" data-toggle="tab" href="#world-tab-19" role="tab" aria-controls="world-tab-19" aria-selected="false">Tuyển sinh</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab20" data-toggle="tab" href="#world-tab-20" role="tab" aria-controls="world-tab-20" aria-selected="false">Tư vấn</a>
                                </li>

                              

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-18" role="tabpanel" aria-labelledby="18">
                                     <div class="row">

                                <?php $__currentLoopData = $giaoduc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$gd->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($gd->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($gd->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $giaoduc_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gd1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$gd1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($gd1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($gd1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="world-tab-19" role="tabpanel" aria-labelledby="tab19">
                                    <div class="row">

                                <?php $__currentLoopData = $tuyensinh; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tsi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$tsi->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tsi->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tsi->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $tuyensinh_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tsi1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$tsi1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tsi1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tsi1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                 <div class="tab-pane fade" id="world-tab-20" role="tabpanel" aria-labelledby="tab20">
                                    <div class="row">

                                <?php $__currentLoopData = $tuvan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$tv->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tv->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tv->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $tuvan_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tv1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$tv1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tv1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tv1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                           <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">CÔNG NGHỆ</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab21" data-toggle="tab" href="#world-tab-21" role="tab" aria-controls="world-tab-21" aria-selected="true">Tất cả</a>
                                </li>

                              
                                <li class="nav-item">
                                    <a class="nav-link" id="tab22" data-toggle="tab" href="#world-tab-22" role="tab" aria-controls="world-tab-22" aria-selected="false">Điện thoại</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab23" data-toggle="tab" href="#world-tab-23" role="tab" aria-controls="world-tab-23" aria-selected="false">Ứng dụng di động</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab24" data-toggle="tab" href="#world-tab-24" role="tab" aria-controls="world-tab-24" aria-selected="false">Máy tính</a>
                                </li>

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-21" role="tabpanel" aria-labelledby="tab21">
                                   <div class="row">

                                <?php $__currentLoopData = $congnghe; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cn): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$cn->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($cn->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($cn->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $congnghe_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cn1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$cn1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($cn1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($cn1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="world-tab-22" role="tabpanel" aria-labelledby="tab22">
                                    <div class="row">

                                <?php $__currentLoopData = $dienthoai; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$dt->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($dt->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($dt->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $dienthoai_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gd1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$dt1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($dt1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($dt1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                       <div class="tab-pane fade" id="world-tab-23" role="tabpanel" aria-labelledby="tab23">
                                    <div class="row">

                                <?php $__currentLoopData = $didong; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$dd->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($dd->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($dd->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $didong_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dd1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$dd1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($dd1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($dd1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                      <div class="tab-pane fade" id="world-tab-24" role="tabpanel" aria-labelledby="tab24">
                                    <div class="row">

                                <?php $__currentLoopData = $maytinh; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mt): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$mt->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($mt->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($mt->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $maytinh_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mt1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$mt1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($mt1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($mt1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                           <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">ÂM NHẠC</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab25" data-toggle="tab" href="#world-tab-25" role="tab" aria-controls="world-tab-25" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab26" data-toggle="tab" href="#world-tab-26" role="tab" aria-controls="world-tab-26" aria-selected="false">Nhạc âu mỹ</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab27" data-toggle="tab" href="#world-tab-27" role="tab" aria-controls="world-tab-27" aria-selected="false">Nhạc hàn</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab28" data-toggle="tab" href="#world-tab-28" role="tab" aria-controls="world-tab-28" aria-selected="false">Nhạc việt</a>
                                </li>

                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                <div class="tab-pane fade show active" id="world-tab-25" role="tabpanel" aria-labelledby="tab25">
                                         
                                    <div class="row">

                                <?php $__currentLoopData = $amnhac; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $am): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$am->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($am->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($am->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $amnhac_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $am1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$am1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($am1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($am1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    
                                </div>
                                </div>

                                <div class="tab-pane fade" id="world-tab-26" role="tabpanel" aria-labelledby="tab26">
                                     <div class="row">

                                <?php $__currentLoopData = $nhacmy; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$nm->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($nm->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($nm->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $nhacmy_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nm1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$nm1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($nm1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($nm1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-27" role="tabpanel" aria-labelledby="tab27">
                                     <div class="row">

                                <?php $__currentLoopData = $nhachan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nh): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$nh->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($nh->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($nh->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $nhachan_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nh1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$nh1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($nh1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($nh1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-28" role="tabpanel" aria-labelledby="tab28">
                                     <div class="row">

                                <?php $__currentLoopData = $nhacviet; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$nv->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($nv->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($nv->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $nhacviet_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nv1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$nv1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($nv1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($nv1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                           <div class="world-catagory-area mt-50 col-md-6">
                            <ul class="nav nav-tabs" id="myTab3" role="tablist">
                                <li class="title">KINH DOANH</li>

                                <li class="nav-item">
                                    <a class="nav-link active" id="tab29" data-toggle="tab" href="#world-tab-29" role="tab" aria-controls="world-tab-29" aria-selected="true">Tất cả</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab30" data-toggle="tab" href="#world-tab-30" role="tab" aria-controls="world-tab-30" aria-selected="false">Tài chính</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab31" data-toggle="tab" href="#world-tab-31" role="tab" aria-controls="world-tab-31" aria-selected="false">Bất động sản</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="tab32" data-toggle="tab" href="#world-tab-32" role="tab" aria-controls="world-tab-32" aria-selected="false">Chứng khoáng</a>
                                </li>

                               
                               
                            </ul>

                            <div class="tab-content" id="myTabContent3">

                                     <div class="tab-pane fade show active" id="world-tab-29" role="tabpanel" aria-labelledby="tab29">
                                     <div class="row">

                                <?php $__currentLoopData = $kinhdoanh; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$kd->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($kd->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($kd->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $kinhdoanh_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kd1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$kd1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($kd1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($kd1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                                   <div class="tab-pane fade" id="world-tab-30" role="tabpanel" aria-labelledby="tab30">
                                     <div class="row">

                                <?php $__currentLoopData = $taichinh; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$tc->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tc->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tc->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $taichinh_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tc1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$tc1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($tc1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($tc1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-31" role="tabpanel" aria-labelledby="tab31">
                                     <div class="row">

                                <?php $__currentLoopData = $bds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$bds->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($bds->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($bds->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $bds_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bds1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$bds1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($bds1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($bds1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>
                                     <div class="tab-pane fade" id="world-tab-32" role="tabpanel" aria-labelledby="tab32">
                                     <div class="row">

                                <?php $__currentLoopData = $chungkhoang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ck): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/800x800/'.$ck->newimg)); ?>" alt="">
                                                    <!-- Catagory -->
                                                    
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($ck->newsname); ?></h5>
                                                    </a>
                                                  
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($ck->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             <?php $__currentLoopData = $chungkhoang_1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ck1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-12 col-md-12">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <img src="<?php echo e(url('/public/img/news/300x300/'.$ck1->newimg)); ?>" alt="">
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="#" class="headline">
                                                        <h5><?php echo e($ck1->newsname); ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <span class="item-meta">
                                                        <a href="#" style="color: #666;"><i class="fa fa-clock-o"></i> <?php echo e($ck1->created_at); ?></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                     
                                    </div>
                                </div>

                            
                            </div>
                        </div>
                        </div>

        
         
            
  

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>