<?php $__env->startSection('title', (!empty($contact)?$contact->seo_title:"")); ?>
<?php $__env->startSection('seo_keyword', (!empty($contact)?$contact->seo_keyword:"")); ?>
<?php $__env->startSection('seo_description', (!empty($contact)?$contact->seo_description:"")); ?>
<?php $__env->startSection('seo_image', (!empty($contact)?asset($contact->seo_image):"")); ?>
<?php $__env->startSection('seo_url', url()->current()); ?>
<?php $__env->startSection('content'); ?>



<div class="container">
    <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
    <li class="breadcrumb-item active" aria-current="page">Tìm kiếm</li>
  </ol>
</nav>
</div>

     <div class="main-content-wrapper section-padding-100">
        <div class="container">
            <div class="row justify-content-center">
                <!-- ============= Post Content Area Start ============= -->
                <div class="col-12 col-lg-8">
                    <div class="post-content-area mb-100">
                        <!-- Catagory Area -->
                        <div class="world-catagory-area">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                            
                                <li class="title">
                                	<?php if(isset($key)): ?>
										<h2><i class="fa fa-list-ul" aria-hidden="true"></i> Kết quả tìm kiếm : <?php echo e($key); ?></h2>
									<?php else: ?>
										<h2><i class="fa fa-list-ul" aria-hidden="true"></i> Các tin tức với tags : <?php echo e($tags); ?></h2>
									<?php endif; ?>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                       

                                <div class="tab-pane fade show active" id="world-tab-1" role="tabpanel" aria-labelledby="tab1">
                                    <!-- Single Blog Post -->
                                    <?php $i=0; ?>
                                 	<?php $__currentLoopData = $news_serch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item_serch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="single-blog-post post-style-4 d-flex align-items-center">
                                        <!-- Post Thumbnail -->
                                        <div class="post-thumbnail">
                                            <img src="<?php echo e(url('/public/img/news/100x100/'.$item_serch->newimg)); ?>" alt="no img">
                                        </div>
                                        <!-- Post Content -->
                                        <div class="post-content">
                                            <a href="<?php echo e(url('chi-tiet/'.$item_serch->slug)); ?>" class="headline" title="">
                                                <h5><?php echo e($item_serch->newsname); ?></h5>
                                            </a>
                                            <p><?php echo e($item_serch->newintro); ?></p>
                                            <!-- Post Meta -->
                                            <span class="item-meta">
												<a href="#"><i class="fa fa-clock-o"></i> <?php echo e($item_serch->created_at); ?></a>
												<a href="<?php echo e(url('/loai-tin/'.$item_serch->list_name($item_serch->idlistnew)['slug'])); ?>">
													<i class="fa fa-list-ul" aria-hidden="true"></i> 
													<?php if($item_serch->list_name($item_serch->idlistnew)['listname'] !=""): ?>
														<?php echo e($item_serch->list_name($item_serch->idlistnew)['listname']); ?>

													<?php else: ?>
														<?php echo e($item_serch->mod_name($item_serch->idmodnew)['modname']); ?>

													<?php endif; ?>
												</a>
											</span>
                                            
                                        </div>
                                    </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                   
                                </div>

                            
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ========== Sidebar Area ========== -->
                <div class="col-12 col-md-8 col-lg-4">
                    <div class="post-sidebar-area">
                        <!-- Widget Area -->
                        <div class="sidebar-widget-area">
                            <h5 class="title">Kết nối với chúng tôi</h5>
                            <div class="widget-content">
                                <p><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgoclamdepcungnang%2F&tabs&width=340&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=439880526654528" width="340" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe></p>
                            </div>
                        </div>
                        <!-- Widget Area -->
                       
                        <div class="sidebar-widget-area">
                            <h5 class="title">Đọc nhiều nhất</h5>
                            <div class="widget-content">
                                <!-- Single Blog Post -->
                                <?php $count =1; ?>
								<?php $__currentLoopData = $most_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item_most): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php if($count <9): ?>
                                <div class="single-blog-post post-style-2 d-flex align-items-center widget-post">
                                    <!-- Post Thumbnail -->
                                    <div class="post-thumbnail">
                                        <img src="<?php echo e(url('/public/img/news/100x100/'.$item_most->newimg)); ?>" alt="">
                                    </div>
                                    <!-- Post Content -->
                                    <div class="post-content">
                                        <a href="<?php echo e(url('chi-tiet/'.$item_most->slug)); ?>" class="headline">
                                            <h5 class="mb-0"><?php echo e($item_most->newsname); ?></h5>
                                        </a>
                                    </div>
                                </div>
                                <?php endif; ?>
								<?php  $count = $count +1; ?>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                               
                            </div>
                        </div>
                        <!-- Widget Area -->
                        <div class="sidebar-widget-area">
                            <h5 class="title">Hợp tác</h5>
                            <div class="widget-content">
                                <div class="social-area d-flex justify-content-between">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                    <a href="#"><i class="fa fa-vimeo"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- Widget Area -->
                
                    </div>
                </div>
            </div>

           
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>