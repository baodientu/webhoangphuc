<!DOCTYPE HTML>
<!-- BEGIN html -->
<html lang = "vi">
	<!-- BEGIN head -->
	<head>
		<title><?php echo $__env->yieldContent('title'); ?></title> 
		<!-- Meta Tags -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="description" content="" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2" />
		<meta name="generator" content="nt7solution.com">
	    <meta property="fb:app_id" content="<?php echo e((!empty($contact)?$contact->fb_app_id:"")); ?>" /> 
	    <meta property="og:type" content="article" /> 
	    <meta property="og:title" content="<?php echo $__env->yieldContent('title'); ?>" />
	    <meta property="og:image" content="<?php echo $__env->yieldContent('seo_image'); ?>" >
	    <meta property="og:description" content="<?php echo $__env->yieldContent('seo_description'); ?>" >
	    <meta property="og:url" content="<?php echo $__env->yieldContent('seo_url'); ?>" />
	    <meta property="og:site_name" content="<?php echo e((!empty($contact)?$contact->seo_title:"")); ?>" />
		<base href="<?php echo e(asset('')); ?> ">
	

		<link rel="icon" href="<?php echo e(url('public/home/img/core-img/favicon.ico')); ?>">

    	<!-- Style CSS -->
    	<link rel="stylesheet" href="<?php echo e(url('public/home/style.css')); ?>">
    	<link rel="stylesheet" href="<?php echo e(url('public/home/css/animate.css')); ?>">
    	<link rel="stylesheet" href="<?php echo e(url('public/home/css/font-awesome.min.css')); ?>">
    	<link rel="stylesheet" href="<?php echo e(url('public/home/css/magnific-popup.css')); ?>">
    	<link rel="stylesheet" href="<?php echo e(url('public/home/css/owl.carousel.css')); ?>">
    	<link rel="stylesheet" href="<?php echo e(url('public/home/css/themify-icons.css')); ?>">
    	<link rel="stylesheet" href="<?php echo e(url('public/home/css/bootstrap.min.css')); ?>">
    	
        <style type="text/css">
            a{
                color: black;
            }
        </style>

    
	</head>

	<!-- BEGIN body -->
	<!-- <body> -->
	<body>
			<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=439880526654528&autoLogAppEvents=1" nonce="tD7I5f45"></script>

        <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" 
        src="https://connect.facebook.net/en_US/sdk.js#xfbml=1
             &version={graph-api-version}
             &appId={your-facebook-app-id}
             &autoLogAppEvents=1" 
        nonce="FOKrbAYI">
  </script>

    <!-- Preloader Start -->
    <div id="preloader">
        <div class="preload-content">
            <div id="world-load"></div>
        </div>
    </div>
    <!-- Preloader End -->

    <!-- ***** Header Area Start ***** -->
      <header class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-lg">
                        <!-- Logo -->
                        <a class="navbar-brand" href=""><img src="<?php echo e(url('public/home/img/core-img/logo2.png')); ?>" alt="Logo"></a>
                        <!-- Navbar Toggler -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#worldNav" aria-controls="worldNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <!-- Navbar -->
                        <div class="collapse navbar-collapse" id="worldNav">
                            <ul class="navbar-nav ml-auto" rel="DANH MỤC TIN">
                               
                                <?php $__currentLoopData = $modnews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $itemmod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="<?php echo e(url('loai-tin').'/'.$itemmod->slug); ?>" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;"><?php echo e($itemmod->modname); ?></a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    	<?php $__currentLoopData = $itemmod->listnews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $itemlist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <a class="dropdown-item" href="<?php echo e(url('loai-tin').'/'.$itemlist->slug); ?>"><?php echo e($itemlist->listname); ?></a>
                       
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </li>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>		
                            </ul>

                           
                            <!-- Search Form  -->
                            <div id="search-wrapper">
                                <form action="<?php echo e(url('/search')); ?>">
                                    <input type="text" id="search" name="key" placeholder="Nhập từ khóa tìm kiếm.." />
                                    <div id="close-icon"></div>
                                    <input class="d-none" type="submit" value="">
                                </form>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ********** Hero Area Start ********** -->
    <div class="hero-area">

        <!-- Hero Slides Area -->
        <div class="hero-slides owl-carousel">
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay" style="background-image: url(public/home/img/blog-img/ba12.jpg);"></div>
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay" style="background-image: url(public/home/img/blog-img/halong.png);"></div>
        </div>

        <!-- Hero Post Slide -->
    
    </div>
    <!-- ********** Hero Area End ********** -->
    	<!-- BEGIN .content -->
			<section class="content">
			<?php echo $__env->yieldContent('content'); ?>				
			<!-- BEGIN .content -->
			</section>
 

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="footer-single-widget">
                        <a href="#"><img src="img/core-img/logo.png" alt=""></a>
                        <div class="copywrite-text mt-30">
                            <p><a class="navbar-brand" href=""><img src="<?php echo e(url('public/home/img/core-img/logo2.png')); ?>" alt="Logo"></a></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="footer-single-widget">
                     
                        <h5>Thông tin liên hệ</h5>

<p>Hà Nội. Hotline: 0919 405 885 | Email: vietnamnetjsc.hn@vietnamnet.vn</p>

<p>Tp.HCM. Hotline: 0919 435 885 | Email: vietnamnetjsc.hcm@vietnamnet.vn</p>

<p>Hỗ trợ kỹ thuật: support@tech.vietnamnet.vn</p>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="footer-single-widget">
                        <h5>Liên hệ chúng tôi</h5>
                        <form action="#" method="post">
                            <input type="email" name="email" id="email" placeholder="Nhập mail của bạn">
                            <button type="button"><i class="fa fa-arrow-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area End ***** -->
    	
    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="<?php echo e(url('public/home/js/jquery/jquery-2.2.4.min.js')); ?>"></script>
    <!-- Popper js -->
    <script src="<?php echo e(url('public/home/js/popper.min.js')); ?>"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo e(url('public/home/js/bootstrap.min.js')); ?>"></script>
    <!-- Plugins js -->
    <script src="<?php echo e(url('public/home/js/plugins.js')); ?>"></script>
    <!-- Active js -->
    <script src="<?php echo e(url('public/home/js/active.js')); ?>"></script>

     <script type="text/javascript">
        var pxtop=200;
        var fixtop=80;
        $(window).scroll(function (){
            if($(this).scrollTop() >= pxtop){
                $('.cc1').fadeIn();
            }
            else{
                $('.cc1').fadeOut();
            }
        })

        $(window).scroll(function(){
            if ($(this).scrollTop() >= fixtop) {
                $('.fixed-top').addClass("fixed");
            }
            else{
                $('.fixed-top').removeClass("fixed");
            }
        })
    </script>

</body>
</html>